import React, { Component } from 'react'

import { Formik, Form, Field } from "formik";
import axios from 'axios';

export default class Login extends Component {

    state = {
        profile: null,
        isLogin: false
    }

    login = async (values) => {
        try {
            const apiUrl = 'https://shop-backendapi.herokuapp.com/api/user/login';
            const response = await axios.post(apiUrl, values);
            // console.log(JSON.stringify(response.data));
            localStorage.setItem('token', JSON.stringify(response.data))

            //get profile
            const apiUrl2 = 'https://shop-backendapi.herokuapp.com/api/user/me';
            const responsrProfile = await axios.get(apiUrl2, {
                headers: {
                    Authorization: 'Bearer ' + response.data.access_token
                }
            })
            // console.log(responsrProfile.data.user)
            localStorage.setItem('profile', JSON.stringify(responsrProfile.data.user))
            this.setState({
                profile: responsrProfile.data.user,
                isLogin: true
            })
        } catch (error) {
            // console.log(error)
            alert(error.response.data.error.message)
            this.setState({
                isLogin: false
            })
        }
    }

    logout = () => {
        localStorage.removeItem('token');localStorage.removeItem('profile');
        this.setState({
            isLogin: false
        })
        // this.props.history.replace({ pathname: '/' })
    }

    componentDidMount() {
        const profile = JSON.parse(localStorage.getItem('profile'));
        if(profile){
            this.setState({
                profile : profile,
                isLogin: true
            })
        }
    }

    render() {
        return (
            <>
                {
                    this.state.isLogin ? (
                        <span className="navbar-text" style={{ fontWeight: '300'}}>
                            ยินดีต้อนรับ คุณ {this.state.profile.name}, Role {this.state.profile.role}
                            <button className="btn btn-danger my-2 my-sm-0 ml-1 btn-sm" onClick={this.logout}>Logout</button>
                        </span>
                    ) : (
                            <Formik
                                onSubmit={(values, { setSubmitting }) => {

                                    this.login(values)

                                    setSubmitting(false);

                                }}
                                initialValues={{
                                    email: '',
                                    password: '',
                                }}
                            >
                                {

                                    ({
                                        handleSubmit,
                                        handleChange,
                                        isSubmitting
                                    }) => (
                                            <Form className="form-inline">


                                                <div className="form-group">
                                                    <Field
                                                        type="email"
                                                        name="email"
                                                        placeholder="Email"
                                                        autoComplete="username"
                                                        className="form-control form-control-sm mr-sm-2"
                                                    />
                                                </div>

                                                <div className="form-group">
                                                    <Field
                                                        type="password"
                                                        name="password"
                                                        placeholder="Password"
                                                        autoComplete="new-password"
                                                        className="form-control form-control-sm mr-sm-2"
                                                    />
                                                </div>

                                                <button
                                                    type="submit"
                                                    className="btn btn-info btn-sm my-2 my-sm-0"
                                                    disabled={isSubmitting}
                                                >
                                                    Log In
                                                </button>


                                            </Form>
                                        )

                                }
                            </Formik>
                        )
                }
            </>
        )
    }
}