import React from 'react';
import { footer } from 'react-bootstrap';

const Footer = () => {
    return (
        <>
            <div className="container">
                <hr />
                <footer className="container">
                    <p>© Company 2017-2019</p>
                </footer>
            </div>
        </>
    )
}

export default Footer
