import React, { Component } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';
import Login from './Login';
import { connect } from 'react-redux';

class NavBar extends Component {
    render() {
        return (
            <Navbar className="py-2" bg="light" variant="light" expand="lg" style={{ borderBottom: '4px solid #416291' }}>
                <Navbar.Brand href="#home" style={{ fontFamily: 'Kanit' }}>สั่งอาหารออนไลน์</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <NavLink className="nav-link" exact={true} activeClassName="active" to="/">
                            <FontAwesomeIcon className="mr-1" icon={faHome} size="1x" color="#416291" />
                            Home
                        </NavLink>
                        <NavLink className="nav-link" activeClassName="active" to="/about">
                            About
                        </NavLink>
                        <NavLink className="nav-link" activeClassName="active" to="/register">
                            Register
                        </NavLink>
                        <NavLink className="nav-link" activeClassName="active" to="/cart">
                            <FontAwesomeIcon className="mr-1" icon={faShoppingCart} />
                            ตระกร้าสินค้า
                            <span className="badge badge-info mx-1" style={{ fontWeight: '500' }}> {this.props.total} </span>
                        </NavLink>
                    </Nav>
                    <Login />
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        total: state.cartReducer.total
    }
}

export default connect(mapStateToProps)(NavBar);