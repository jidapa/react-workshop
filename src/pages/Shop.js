import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import defualtFood from '../image/dish.png';
import { addToCart } from '../redux/actions/cartAction';

class Shop extends Component {
    state = {
        shop: {},
        menus: [],
        location: {
            lat: 0,
            lgn: 0
        }
    }

    async componentDidMount() {
        const id = this.props.match.params.id;
        const response = await axios.get('https://shop-backendapi.herokuapp.com/api/shop/' + id);
        this.setState({
            shop: response.data.data,
            menus: response.data.data.menus,
            location: response.data.data.location
        })
    }

    addToCart = (menu) => {
        const item = {
            id: menu._id,
            name: menu.name,
            price: menu.price.$numberDecimal,
            qty: 1
        }
        this.props.dispatch(addToCart(item, this.props.cart));
    }

    render() {
        return (
            <>
                <div className="container my-5">
                    <div className="row">
                        <div className="col-md-10" style={{ fontWeight: '600', fontSize: '2em' }}>
                            {this.state.shop.name}
                        </div>
                    </div>
                    <div className="row mt-1">
                        {
                            this.state.menus.map((menu) => {
                                return (
                                    <div className="col-md-4" key={menu._id}>
                                        <div className="card mt-2" >
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-3">
                                                        <img src={defualtFood} alt="defualtFood" style={{ width: '4.8rem' }} />
                                                    </div>
                                                    <div className="col-md-9">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <span style={{ fontWeight: '500' }}>ชื่อ:</span><span style={{ fontWeight: '300' }}> {menu.name}</span>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <span style={{ fontWeight: '400' }}>ราคา:</span> <span style={{ fontWeight: '300' }}>{menu.price.$numberDecimal} บาท </span>
                                                            </div>
                                                        </div>
                                                        <div className="row mt-2">
                                                            <div className="col-md-12">
                                                                <button onClick={() => {this.addToCart(menu)}} className="btn btn-info">ซื้อเลย</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cartReducer.cart
    }
}

export default connect(mapStateToProps)(Shop)