import React, { Component } from 'react'

export default class NotFound extends Component {
    render() {
        return (
            <>
                <div className="container">
                    <div className="row my-5">
                        <div className="col-md-12">
                            <p className="alert alert-danger">404 Not Found :(</p>
                        </div>
                    </div>
                </div>

            </>
        )
    }
}
