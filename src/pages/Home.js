import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faHeart } from '@fortawesome/free-solid-svg-icons';

export default class Home extends Component {
    CancelToken = axios.CancelToken;
    source = this.CancelToken.source();
    state = {
        shops: [],
        loading: true
    }

    async getData() {
        try {
            const response = await axios.get('https://shop-backendapi.herokuapp.com/api/shop', {
                cancelToken: this.source.token
            });
            this.setState({
                shops: response.data.data,
                loading: false
            });
        }
        catch (error) {
            if (axios.isCancel(error)) {
                console.log('home cancelled')
            }
            else {
                console.log(error);
            }
        }
    }

    componentDidMount() {
        this.getData();
    }

    componentWillUnmount() {
        this.source.cancel();
    }

    render() {
        return (
            <>
                <div className="jumbotron">
                    <div className="container">
                        <h1 className="display-3">เลือกร้านที่คุณถูกใจ....</h1>
                        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
                        {/* <p><a className="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p> */}
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            {
                                this.state.loading === true && (
                                    <div className="spinner-grow" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                    <div className="row">
                        {
                            this.state.shops.map((shop) => {
                                return (
                                    <div className="col-md-4" key={shop.id}>
                                        <div className="Card mb-3">
                                            <img src={shop.photo} alt={shop.name} className="card-img-top" height="225" />
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <h5 className="card-title" style={{ fontWeight: '300' }}>
                                                            {shop.name}
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <Link className="btn btn-info btn-sm w-100" style={{ fontWeight: '500' }}
                                                            to={
                                                                { pathname: '/shop/' + shop.id }
                                                            }>
                                                            View Menu
                                                    </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>

            </>
        )
    }
}
