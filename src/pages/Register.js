import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faEnvelope, faKey } from '@fortawesome/free-solid-svg-icons';
import * as Yup from 'yup';
import axios from 'axios';

const RegisterSchema = Yup.object().shape({
    name: Yup.string()
        .required('Fullname is Required'),
    password: Yup.string()
        .min(3, 'Please Enter less then 3 letters')
        .required('Password is Required'),
    email: Yup.string()
        .email('Invalid email')
        .required('Email is Required'),
});

export default class Register extends Component {
    reguster = async (values) => {
        try {
            const apiUrl = 'https://shop-backendapi.herokuapp.com/api/user/register';
            const response = await axios.post(apiUrl, values);
            alert(response.data.message);

            this.props.history.replace({ pathname: '/' }); //สมัครเสร็จแล้วให้กลับไปที่หน้าแรก
        } catch (error) {
            // console.log(error)
            alert(error.response.data);
        }
    }

    render() {
        return (
            <>
                <div className="container my-5">
                    <div className="row">
                        <div className="col-md-12">
                            <Formik
                                initialValues={{ name: '', email: '', password: '' }}
                                validationSchema={RegisterSchema}
                                onSubmit={(values, { setSubmitting }) => {
                                    this.reguster(values)
                                    setSubmitting(false)
                                }}
                            >
                                {({ isSubmitting, touched, errors }) => (
                                    <Form noValidate>
                                        <div className="row justify-content-center">
                                            <div className="col-md-4">
                                                <span style={{ fontSize: '2em', fontWeight: '500' }}>Register</span>
                                            </div>
                                        </div>
                                        <div className="row justify-content-center">
                                            <div className="col-md-4">
                                                <label htmlFor="name">Fullname</label>
                                                <div className="input-group input-group-sm mb-3">
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text"><FontAwesomeIcon className="mr-1" icon={faUser} /> </span>
                                                    </div>
                                                    <Field
                                                        type="text"
                                                        name="name"
                                                        placeholder="Name"
                                                        className={
                                                            `form-control ${touched.name && errors.name ? 'is-invalid' : ''}`
                                                        }
                                                    />
                                                    <ErrorMessage name="name" className="invalid-feedback" component="div" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row justify-content-center">
                                            <div className="col-md-4">
                                                <label htmlFor="name">Email</label>
                                                <div className="input-group input-group-sm mb-3">
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text"><FontAwesomeIcon className="mr-1" icon={faEnvelope} /> </span>
                                                    </div>
                                                    <Field
                                                        type="email"
                                                        name="email"
                                                        placeholder="Email"
                                                        className={
                                                            `form-control ${touched.email && errors.email ? 'is-invalid' : ''}`
                                                        }
                                                    />
                                                    <ErrorMessage name="email" className="invalid-feedback" component="div" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row justify-content-center">
                                            <div className="col-md-4">
                                                <label htmlFor="name">Password</label>
                                                <div className="input-group input-group-sm mb-3">
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text"><FontAwesomeIcon className="mr-1" icon={faKey} /> </span>
                                                    </div>
                                                    <Field
                                                        type="password"
                                                        name="password"
                                                        placeholder="Password"
                                                        className={
                                                            `form-control ${touched.password && errors.password ? 'is-invalid' : ''}`
                                                        }
                                                    />
                                                    <ErrorMessage name="password" className="invalid-feedback" component="div" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row justify-content-center">
                                            <div className="col-md-4">
                                                <button type="submit" disabled={isSubmitting} className="btn btn-info">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
