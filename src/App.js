import React from 'react';
import { BrowserRouter as Router, Route,  Switch } from "react-router-dom";
import { Provider } from 'react-redux';
//import { createStore } from 'redux'
//import  rootReducer from './redux/reducers/index'

import { PersistGate } from 'redux-persist/integration/react'
import configureStore from './redux/configureStore'


// import PrivateRoute from './guard/auth';

import './App.css';
import Home from './pages/Home';
import About from './pages/About';
import Shop from './pages/Shop';
import Cart from './pages/Cart';

import NavBar from './components/NavBar';
import Footer from './components/Footer';
import NotFound from './pages/NotFound';
import Register from './pages/Register';

// const store = createStore(rootReducer);
const { store, persistor } = configureStore();

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>

          <NavBar />

          <main role="main">

            <Switch>
              <Route exact path="/" component={Home} />
              {/* <PrivateRoute path="/about" component={About} /> */}
              <Route path="/about" component={About} />
              <Route path="/register" component={Register} />
              <Route path="/cart" component={Cart} />
              <Route path="/shop/:id" component={Shop} />

              <Route component={NotFound} />
            </Switch>

          </main>

          <Footer />

        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
