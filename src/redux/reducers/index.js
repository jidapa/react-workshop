import { combineReducers } from 'redux';
import cartReducer from './cartReducer'
//list reducer
const rootReducer = combineReducers({
    cartReducer
})

export default rootReducer