
//ชื่อ action
export const ADD_CART = 'ADD_CART';

//สร้างfunction
export const addToCart = (menu = {}, cart = []) => {
    //เช็คซ้ำ
    let exists = false;

    //ถ้าตระกร้าสิ้นคเาที่ส่งมามีข้อมูลแล้วให้เช็กซ้ำ
    if(cart.length > 0){
        for (const c of cart) {
            if(c.id === menu.id){
                c.qty++;
                exists = true;
            }
        }
    }

    if(!exists){
         //ใส่เมนูเข้าไปใน cart
        cart.push(menu);
    }

    //function reduce เอาไว้หาผลรวม เป็นการบวกทบ โดย 0 คือค่าเริ่มต้นของการsum
    const total = cart.reduce((total, value) => total + value.qty, 0);

    //ตัวนี้จะ return ไปให้ reducer
    return {
        type: ADD_CART,
        payload: {
            cart: cart,
            total: total
        }
    }
}